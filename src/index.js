import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App.jsx";
import "./increment";

ReactDOM.render(<App />, document.getElementById("root"));
