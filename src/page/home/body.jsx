import { Box, Grid, makeStyles, Card, CardContent } from "@material-ui/core";
import { useState } from "react";
import { useHistory } from "react-router-dom";
import { ArrowUpward } from "@material-ui/icons";
import { Link as SLink } from "react-scroll";
import { Header } from "./header";

import AOS from "aos";
import "aos/dist/aos.css";

AOS.init({
  once: false,
  easing: "linear",
  duration: 500,
});

const styles = makeStyles(() => ({
  card_w: {
    display: "flex",
  },
  s_view: {
    position: "relative",
    "&:hover": {
      "& img": {
        opacity: 0.3,
      },
      "& div": {
        opacity: 1,
      },
    },
  },
  img_c: {
    maxWidth: "430px",
    transition: ".5s ease",
    backfaceVisibility: "hidden",
  },
  view: {
    position: "absolute",
    opacity: 0,
    top: "35%",
    left: "50%",
    textAlign: "center",
  },
  t_view: {
    cursor: "pointer",
    backgroundColor: "gray",
    color: "white",
    padding: 20,
    fontSize: 20,
    fontWeight: "bold",
    fontFamily: "bitter",
  },
  t_gallery: {
    backgroundColor: "rgba(0,0,0,.6)",
    color: "white",
    padding: 30,
    fontFamily: "bitter",
    fontSize: 35,
    borderRadius: "10px",
    transition: ".3s ease",
    textTransform: "toUpperCase",
    "&:hover": {
      boxShadow: "0px 0px 3px 3px black",
    },
    cursor: "pointer",
  },
  s_gall: {
    "&:hover": {
      "& img": {
        opacity: 0.7,
      },
    },
  },
  up_btn: {
    position: "fixed",
    top: "80%",
    right: "5%",
    backgroundColor: "blue",
    borderRadius: "50%",
    padding: 10,
    color: "white",
    transition: "ease .4s",
    cursor: "pointer",
    "&:hover": {
      boxShadow: "0px 0px 5px 5px",
    },
  },
}));

export const Body = () => {
  const [up, setUp] = useState(false);
  const classes = styles();
  const history = useHistory();

  window.onscroll = () => {
    window.pageYOffset > 200
      ? setUp(true)
      : window.pageYOffset === 0 && setUp(false);
  };

  const handleClick = () => {
    history.push("/gallery");
  };
  return (
    <>
      <Header />
      <Box paddingTop={2} paddingBottom={9.7} id="service">
        <p
          style={{
            fontFamily: "ds",
            fontSize: 60,
            textAlign: "center",
          }}
          data-aos="fade-in"
        >
          Services
        </p>
        <hr style={{ width: "5%", color: "black" }} />

        <Grid
          container
          style={{ paddingTop: 30 }}
          justify="center"
          data-aos="fade-up"
        >
          <Grid item xs={12} md={6} sm={6} lg={6}>
            <Card className={classes.card_w}>
              <Box display="flex" justifyContent="center" alignItems="center">
                <Grid container justify="center" alignItems="center">
                  <Grid item lg={6} md={6} xs={12}>
                    <CardContent style={{ textAlign: "center" }}>
                      <p style={{ fontFamily: "bitter", fontSize: 25 }}>
                        Camera
                      </p>
                      <hr style={{ width: "10%" }} />
                      <p style={{ fontFamily: "bitter", fontSize: 25 }}>
                        Video
                      </p>
                    </CardContent>
                  </Grid>
                  <Grid item lg={6} md={6} xs={12} className={classes.s_view}>
                    <img
                      src="/assets/images/cam.jpg"
                      className={classes.img_c}
                      alt="cam"
                    />
                    <Box className={classes.view}>
                      <p className={classes.t_view}>Sample</p>
                    </Box>
                  </Grid>
                </Grid>
              </Box>
            </Card>
          </Grid>
          <Grid item xs={12} md={6} sm={6} lg={6}>
            <Card>
              <Grid container justify="center" alignItems="center">
                <Grid item lg={6} md={6} xs={12}>
                  <CardContent style={{ textAlign: "center" }}>
                    <p style={{ fontFamily: "bitter", fontSize: 25 }}>Drone</p>
                    <hr style={{ width: "10%" }} />
                  </CardContent>
                </Grid>
                <Grid item lg={6} md={6} xs={12} className={classes.s_view}>
                  <img
                    src="/assets/images/drone.jpg"
                    className={classes.img_c}
                    alt="drone"
                  />
                  <Box className={classes.view}>
                    <p className={classes.t_view}>Sample</p>
                  </Box>
                </Grid>
              </Grid>
            </Card>
          </Grid>
        </Grid>
        <Box paddingTop={5}>
          <p
            id="gallery"
            data-aos="zoom-in"
            onClick={handleClick}
            style={{ fontFamily: "ds", fontSize: 60, textAlign: "center" }}
          >
            Gallery
          </p>
          <hr style={{ width: "5%", color: "black" }} />
          <Grid style={{ paddingTop: 50 }} container alignItems="center">
            <Grid
              item
              xs={12}
              md={4}
              sm={4}
              lg={4}
              className={classes.s_gall}
              style={{ position: "relative" }}
              data-aos="flip-left"
            >
              <img
                className={classes.img_c}
                style={{ filter: "blur(2px)" }}
                src="/assets/images/camera/nature/nature1.jpg"
                alt="nature1"
              />
              <Box position="absolute" top="40%" left="30%">
                <p onClick={handleClick} className={classes.t_gallery}>
                  Nature
                </p>
              </Box>
            </Grid>
            <Grid
              item
              xs={12}
              md={4}
              sm={4}
              lg={4}
              style={{ position: "relative" }}
              className={classes.s_gall}
              data-aos="flip-right"
            >
              <img
                className={classes.img_c}
                style={{ filter: "blur(2px)" }}
                src="/assets/images/camera/street/street1.jpg"
                alt="street1"
              />
              <Box position="absolute" top="40%" left="30%">
                <p onClick={handleClick} className={classes.t_gallery}>
                  Street
                </p>
              </Box>
            </Grid>
            <Grid
              item
              xs={12}
              md={4}
              sm={4}
              lg={4}
              style={{ position: "relative" }}
              className={classes.s_gall}
              data-aos="flip-left"
            >
              <img
                className={classes.img_c}
                style={{ filter: "blur(2px)" }}
                src="/assets/images/camera/people/people1.jpg"
                alt="people1"
              />
              <Box position="absolute" top="40%" left="30%">
                <p onClick={handleClick} className={classes.t_gallery}>
                  People
                </p>
              </Box>
            </Grid>
          </Grid>
        </Box>
        <Box paddingTop={5}>
          <p
            id="gallery"
            data-aos="zoom-in"
            onClick={handleClick}
            style={{ fontFamily: "ds", fontSize: 60, textAlign: "center" }}
          >
            Client
          </p>
          <hr style={{ width: "5%", color: "black" }} />
          <Box paddingTop={2.5}>
            <Grid
              container
              style={{ textAlign: "center" }}
              justify="space-around"
              alignItems="center"
              data-aos="zoom-out"
            >
              <Grid
                item
                lg={3}
                xs={11}
                md={3}
                sm={5}
                style={{ paddingBottom: 30 }}
              >
                <Box
                  color="white"
                  style={{ backgroundColor: "rgba(0,0,0,.7)" }}
                  paddingTop={3}
                  paddingBottom={3}
                  borderRadius="20px"
                >
                  <p style={{ fontSize: "3vw", fontFamily: "bitter" }}>
                    NUMBER
                  </p>
                  <p>
                    <span
                      style={{
                        fontWeight: "bold",
                        color: "black",
                        fontSize: 40,
                        fontFamily: "ds",
                        paddingRight: 10,
                      }}
                      className="numscroller"
                      data-min="0"
                      data-max="18263"
                      data-delay="1"
                      data-increment="20"
                    >
                      0
                    </span>
                    <i>persons</i>
                  </p>
                </Box>
              </Grid>
              <Grid
                item
                lg={3}
                xs={11}
                md={3}
                sm={5}
                style={{ paddingBottom: 30 }}
              >
                <Box
                  color="white"
                  style={{ backgroundColor: "rgba(0,0,0,.7)" }}
                  paddingTop={3}
                  paddingBottom={3}
                  borderRadius="20px"
                >
                  <p style={{ fontSize: "3vw", fontFamily: "bitter" }}>
                    SATISFACTION
                  </p>
                  <p>
                    <span
                      style={{
                        fontWeight: "bold",
                        color: "black",
                        fontSize: 40,
                        fontFamily: "ds",
                        paddingRight: 10,
                      }}
                      className="numscroller"
                      data-min="0"
                      data-max="100"
                      data-delay="5.5"
                      data-increment="1"
                    >
                      0
                    </span>
                    <i>%</i>
                  </p>
                </Box>
              </Grid>
              <Grid item lg={3} xs={11} sm={8} md={3}>
                <Box
                  color="white"
                  style={{ backgroundColor: "rgba(0,0,0,.7)" }}
                  paddingTop={3}
                  paddingBottom={3}
                  borderRadius="20px"
                >
                  <p style={{ fontSize: "3vw", fontFamily: "bitter" }}>
                    QUALITY
                  </p>
                  <p>
                    <span
                      style={{
                        fontWeight: "bold",
                        color: "black",
                        fontSize: 40,
                        fontFamily: "ds",
                        paddingRight: 10,
                      }}
                      className="numscroller"
                      data-min="0"
                      data-max="100"
                      data-delay="5.5"
                      data-increment="1"
                    >
                      0
                    </span>
                    <i>%</i>
                  </p>
                </Box>
              </Grid>
            </Grid>
          </Box>
        </Box>
      </Box>
      {up && (
        <SLink
          className={classes.up_btn}
          to="header"
          smooth={true}
          spy={true}
          duration={1000}
        >
          <ArrowUpward />
        </SLink>
      )}
    </>
  );
};
