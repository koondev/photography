import {
  Box,
  makeStyles,
  useTheme,
  useMediaQuery,
  CircularProgress,
  Button,
} from "@material-ui/core";
import { useState } from "react";
import image from "../../assets/image/image.json";
import InfiniteScroll from "react-infinite-scroll-component";
import { FavoriteBorder, ArrowDownward, Favorite } from "@material-ui/icons";
import nature from "../../assets/image/nature.json";
import street from "../../assets/image/stree.json";
import people from "../../assets/image/people.json";
import { Link } from "react-router-dom";

const style = {
  c_img: {
    flex: "18%",
    maxWidth: "30%",
    padding: "0 4px",
  },
  c_img_r: {
    flex: "40%",
    maxWidth: "50%",
    padding: "0 4px",
  },
};

const classes = makeStyles(() => ({
  icon: {
    backgroundColor: "rgba(0,0,0,.6)",
    width: "97%",
    paddingBottom: 20,
    opacity: 0,
  },
  contain: {
    transition: "ease .5s",
    "& img": {
      transition: "ease .5s",
    },
    "&:hover": {
      "& img": {
        opacity: 0.5,
      },
      "& div": {
        opacity: 1,
      },
    },
  },
  i_down: {
    marginLeft: 160,
    marginTop: 20,
    color: "white",
    cursor: "pointer",
  },
  i_fav: {
    marginLeft: 30,
    marginTop: 20,
    color: "white",
    cursor: "pointer",
  },
  faved: {
    marginLeft: 30,
    marginTop: 20,
    color: "pink",
    cursor: "pointer",
  },
}));

export const Gallery = () => {
  const [items, setItems] = useState(image);
  const [hasMore, setMore] = useState(true);
  const [categ, setCateg] = useState({
    natures: false,
    streets: false,
    peoples: false,
  });
  const [fav, setFav] = useState(false);
  const classeur = classes();
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.only("xs"));

  const loadMore = () => {
    if (items.length > 200) {
      setMore(false);
      return;
    } else {
      setTimeout(() => {
        setItems(
          items.concat(
            categ.natures
              ? nature
              : categ.peoples
              ? people
              : categ.streets
              ? street
              : image
          )
        );
      }, 2000);
    }
  };

  return (
    <Box>
      <Box
        paddingBottom={3}
        display="flex"
        justifyContent="space-around"
        position="sticky"
        top={10}
        zIndex={1}
      >
        <Link to="/">
          <Button color="primary" variant="contained">
            Home
          </Button>
        </Link>
        <Button
          onClick={() => {
            setCateg({ natures: false, streets: false, peoples: false });
            setItems(image);
          }}
          color="info"
          variant="contained"
        >
          <p style={{ fontWeight: "bold", fontFamily: "bitter" }}>ALL</p>
        </Button>
        <Button
          onClick={() => {
            setCateg({ natures: true, streets: false, peoples: false });
            setItems(nature);
          }}
          color="secondary"
          variant="contained"
        >
          <p style={{ fontWeight: "bold", fontFamily: "bitter" }}>Nature</p>
        </Button>
        <Button
          onClick={() => {
            setCateg({ natures: false, streets: true, peoples: false });
            setItems(street);
          }}
          color="secondary"
          variant="contained"
        >
          <p style={{ fontWeight: "bold", fontFamily: "bitter" }}>Street</p>
        </Button>

        <Button
          onClick={() => {
            setCateg({ natures: false, streets: false, peoples: true });
            setItems(people);
          }}
          color="secondary"
          variant="contained"
        >
          <p style={{ fontWeight: "bold", fontFamily: "bitter" }}>People</p>
        </Button>
      </Box>
      <Box>
        <InfiniteScroll
          dataLength={items.length}
          next={loadMore}
          hasMore={hasMore}
          loader={
            <h4 style={{ width: "100%", textAlign: "center" }}>
              <CircularProgress />
            </h4>
          }
          endMessage={
            <h4 style={{ width: "100%", textAlign: "center", color: "grey" }}>
              No more .
            </h4>
          }
          style={{ display: "flex", flexWrap: "wrap", padding: "0 4px" }}
        >
          {items.map((item, index) => {
            return (
              <Box
                className={classeur.contain}
                position="relative"
                key={index}
                style={!matches ? style.c_img : style.c_img_r}
              >
                <Box position="absolute" className={classeur.icon} zIndex={1}>
                  {fav ? (
                    <Favorite
                      onClick={() => setFav(false)}
                      className={classeur.faved}
                    />
                  ) : (
                    <FavoriteBorder
                      onClick={() => setFav(true)}
                      className={classeur.i_fav}
                    />
                  )}
                  <a href={item.src} className={classeur.i_down} download>
                    <ArrowDownward
                      style={{
                        border: "1px solid white",
                        borderRadius: "20px",
                      }}
                    />
                  </a>
                </Box>

                <img src={item.src} width="100%" alt="" />
              </Box>
            );
          })}
        </InfiniteScroll>
      </Box>
    </Box>
  );
};
