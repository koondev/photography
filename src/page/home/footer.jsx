import {
  Announcement,
  People,
  Contacts,
  Phone,
  Home,
  Facebook,
  GitHub,
} from "@material-ui/icons";
import {
  Box,
  Container,
  Grid,
  Button,
  TextField,
  makeStyles,
} from "@material-ui/core";

const styles = makeStyles(() => ({
  link: {
    color: "white",
    paddingRight: 10,
    cursor: "pointer",
  },
  t_footer: {
    fontFamily: "bitter",
    fontSize: 25,
  },
}));

export const Footer = () => {
  const classes = styles();
  return (
    <Box
      style={{
        paddingTop: 10,
        paddingBottom: 20,
        backgroundColor: "black",
        color: "white",
      }}
      id="footer"
    >
      <Container>
        <Grid container justify="center" style={{ textAlign: "center" }}>
          <Grid
            item
            xs={12}
            md={4}
            sm={6}
            lg={4}
            style={{ paddingTop: 30, width: 500 }}
          >
            <Box display="flex" alignItems="center" justifyContent="center">
              <p>Subscribe to news letters</p>{" "}
              <Announcement style={{ paddingLeft: 10 }} />
            </Box>
            <hr style={{ width: "10%" }} />
            <br />

            <TextField
              variant="filled"
              style={{ backgroundColor: "grey", width: 300 }}
              label="Email adress"
              name="email"
            />
            <br />
            <Box paddingTop={2}>
              <Button color="primary" variant="contained">
                Subscribe
              </Button>
            </Box>
          </Grid>
          <Grid
            item
            xs={12}
            md={4}
            sm={6}
            lg={4}
            style={{ alignSelf: "flex-start", paddingTop: 30 }}
          >
            <Box display="flex" flexDirection="column">
              <Box display="flex" alignItems="center" justifyContent="center">
                <p>Social</p> <People style={{ paddingLeft: 10 }} />
              </Box>
              <hr style={{ width: "10%" }} />
              <Box display="flex" justifyContent="center" paddingTop={3}>
                <a
                  className={classes.link}
                  href="https://gitlab.com/koondev"
                  target="_blank"
                  rel="noreferrer"
                >
                  <GitHub style={{ fontSize: 60 }} />
                </a>
                <a
                  className={classes.link}
                  href="https://facebook.com/danslezone"
                  target="_blank"
                  rel="noreferrer"
                >
                  <Facebook style={{ fontSize: 60 }} />
                </a>
              </Box>
            </Box>
          </Grid>
          <Grid
            item
            xs={12}
            md={4}
            sm={12}
            style={{ alignSelf: "flex-start", paddingTop: 30 }}
            lg={4}
          >
            <Box>
              <Box display="flex" alignItems="center" justifyContent="center">
                <p>Contact</p> <Contacts style={{ paddingLeft: 10 }} />
              </Box>
              <hr style={{ width: "10%" }} />
              <br />
              <Box paddingTop={2}>
                <Box display="flex" alignItems="center" justifyContent="center">
                  <Phone style={{ paddingRight: 10 }} /> +26130000000
                </Box>
                <Box
                  display="flex"
                  alignItems="center "
                  justifyContent="center"
                >
                  <Home style={{ paddingRight: 10 }} /> Rue Analakely 30bis ,
                  A80
                </Box>
              </Box>
            </Box>
          </Grid>
        </Grid>
      </Container>
    </Box>
  );
};
