import {
  Box,
  Button,
  Container,
  makeStyles,
  useTheme,
  useMediaQuery,
} from "@material-ui/core";
import { ExitToApp, Menu } from "@material-ui/icons";
import { Link as SLink } from "react-scroll";
import { Link } from "react-router-dom";

const styles = makeStyles(() => ({
  link: {
    color: "white",
    textDecoration: "none",
    paddingRight: 50,
    fontWeight: "bold",
    fontFamily: "ds",
    fontSize: 25,
    paddingLeft: 10,
    "&:hover": {
      borderLeft: "1px solid white",
    },
    cursor: "pointer",
  },

  logo: {
    fontWeight: "bold",
    fontFamily: "ds",
    border: "2px solid white",
    padding: 10,
  },
}));

export const Header = () => {
  const theme = useTheme();
  const matches = useMediaQuery(theme.breakpoints.up("md"));
  const classes = styles();

  return (
    <Box
      id="header"
      color="white"
      style={{
        backgroundColor: "black",
        height: "88vh",
      }}
      paddingBottom={!matches ? 8 : 9.5}
    >
      <Container>
        <Box display="flex" justifyContent="space-between" alignItems="center">
          <Box fontSize={25}>
            <p className={classes.logo}>Imag</p>
          </Box>
          <Box display="flex" alignItems="center">
            {matches ? (
              <>
                <SLink
                  to="service"
                  className={classes.link}
                  spy={true}
                  smooth={true}
                  duration={1000}
                >
                  Services
                </SLink>
                <Link to="/gallery" className={classes.link}>
                  Gallery
                </Link>
                <SLink
                  to="footer"
                  className={classes.link}
                  spy={true}
                  smooth={true}
                  duration={1000}
                >
                  Contact
                </SLink>
              </>
            ) : (
              <Menu style={{ paddingRight: 20, fontSize: 35 }} />
            )}

            <Box
              display="flex"
              alignItems="center"
              style={{
                borderLeft: "1px solid white",
                paddingLeft: 30,
                cursor: "pointer",
              }}
            >
              {!matches ? null : (
                <p
                  style={{
                    paddingRight: 10,
                    fontFamily: "ds",
                    fontSize: 25,
                    fontWeight: "bold",
                  }}
                >
                  Sign in
                </p>
              )}
              <ExitToApp style={!matches ? { fontSize: 30 } : {}} />
            </Box>
          </Box>
        </Box>
      </Container>
      <Box textAlign="center" color="white" paddingTop={matches ? 20 : 5}>
        <Box
          fontSize={70}
          style={{
            fontFamily: "bitter",
            fontWeight: "bold",
            animation:
              "logoAnim 0.7s cubic-bezier(0.470, 0.000, 0.745, 0.715) both",
          }}
        >
          THE MOST CREATIVE
        </Box>
        <hr style={{ width: 70 }} />
        <Box style={{ animation: "formAnim 3s" }}>
          <p
            style={{
              fontFamily: "bitter",
              letterSpacing: 10,
            }}
          >
            Capture your best moments
          </p>
          <Box display="flex" paddingTop={3} justifyContent="center">
            <Box paddingRight={2}>
              <Button variant="contained">
                <SLink to="service" spy={true} smooth={true} duration={1000}>
                  Services
                </SLink>
              </Button>
            </Box>
            <Button variant="contained">
              <SLink to="footer" spy={true} smooth={true} duration={1000}>
                Contact us
              </SLink>
            </Button>
          </Box>
        </Box>
      </Box>
    </Box>
  );
};
