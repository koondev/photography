export const NotFound = () => {
  return (
    <div style={{ position: "absolute", top: "50%", left: "50%" }}>
      404 Page Not Found
    </div>
  );
};
