import { BrowserRouter, Route, Switch } from "react-router-dom";
import Home from "./page/home/home";
import { NotFound } from "./page/404";
import { Gallery } from "./page/home/gallery";
import { Footer } from "./page/home/footer";

const App = () => {
  return (
    <BrowserRouter>
      <Switch>
        <Route path="/" exact children={<Home />} />
        <Route path="/gallery" children={<Gallery />} />
        <Route path="" children={<NotFound />} />
      </Switch>
      <Footer />
    </BrowserRouter>
  );
};

export default App;
